# NPM Update (npmqu)

An extremely simple npm update script to force update of all packages.  
  
Install package  
```npm i -g npmqu```

Run the command to update all packages:
```npmqu```  

TODO:
 - Add more control / show a list to control the update process
 - Show current vs updatable versions

Log issues/features here: [https://gitlab.com/BetterCorp/public/npm-updater/-/issues](https://gitlab.com/BetterCorp/public/npm-updater/-/issues)