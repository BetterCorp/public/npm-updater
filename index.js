const fs = require('fs');
const path = require('path');
const nodeCmd = require('node-cmd');

const cwd = process.cwd();

const pkJsonPath = path.join(cwd, './package.json');

if (!fs.existsSync(pkJsonPath)) {
  throw 'Cannot find package.json: ' + pkJsonPath;
}

let pkJson = JSON.parse(fs.readFileSync(pkJsonPath).toString());

let dependencies = Object.keys(pkJson.dependencies || {});
let devDependencies = Object.keys(pkJson.devDependencies || {});

console.log(`dependencies: ${dependencies.join(',')}`);
console.log(`devDependencies: ${devDependencies.join(',')}`);


let runScript = [`npm remove ${devDependencies.join(' ')} ${dependencies.join(' ')}`, `npm install --save-dev ${devDependencies.join(' ')}`, `npm install --save ${dependencies.join(' ')}; `];

for (let script of runScript) {
  console.log(script);
  let result = nodeCmd.runSync(script);
  console.log(result.err || result.stderr || result.data || 'Run Complete');
}

console.log('COMPLETE');